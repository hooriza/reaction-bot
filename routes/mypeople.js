var receiver = require("../mypeople/receiver");

// 알림 콜백 예제

// ----------------------------------------------------------
//  1:1 대화
// ----------------------------------------------------------
// { content: '메세지',
//  buddyId: 'BU_iOYEwdlbrLsZqM14QZaXVw00',
//  groupId: '',
//  action: 'sendFromMessage' }

//  대화(이미지)
// { content: 'myp_pci:51A488B20545D3001C',
//  buddyId: 'BU_iOYEwdlbrLsZqM14QZaXVw00',
//  groupId: '',
//  action: 'sendFromMessage' }

// 친구 추가
// { content: '[{"buddyId":"BU__BFOct2CNgszbvBC-NGy9g00","isBot":"N","name":"사용자","photoId":"myp_pub:51A31B8407666B0009"}]',
//  buddyId: 'BU__BFOct2CNgszbvBC-NGy9g00',
//  groupId: '',
//  action: 'addBuddy' }

// ----------------------------------------------------------
//  그 룹
// ----------------------------------------------------------
// 
// 그룹 : 봇 입장
// { content: '[{"buddyId":"BU_vy8zKcwnpj5UPJ6HXnSF9w00","isBot":"Y","name":"똑똑박사","photoId":"myp_pub:519F261E070D890002"}]',
//  buddyId: 'BU_iOYEwdlbrLsZqM14QZaXVw00',
//  groupId: 'GID_ihTl1',
//  action: 'inviteToGroup' }

// 그룹 : 대화
//  { content: '메세지',
//   buddyId: 'BU_iOYEwdlbrLsZqM14QZaXVw00',
//   groupId: 'GID_ihTl1',
//   action: 'sendFromGroup' }

// 그룹 : 대화(이미지)
//  { content: 'myp_pci:51A48697032B550009',
//   buddyId: 'BU_iOYEwdlbrLsZqM14QZaXVw00',
//   groupId: 'GID_ihTl1',
//   action: 'sendFromGroup' }

// 그룹 : 초대(봇)
//  { content: '[{"buddyId":"BU_vy8zKcwnpj5UPJ6HXnSF9w00","isBot":"Y","name":"봇이름","photoId":"myp_pub:519F261E070D890002"}]',
//   buddyId: 'BU_iOYEwdlbrLsZqM14QZaXVw00',
//   groupId: 'GID_n3dl1',
//   action: 'inviteToGroup' }

// 그룹 : 초대
//  { content: '[{"buddyId":"BU_1M9qHDITjlU0","isBot":"N","name":"초대자","photoId":"myp_pub:4FE5829B04234E0031"}]',
//   buddyId: 'BU_iOYEwdlbrLsZqM14QZaXVw00',
//   groupId: 'GID_n3dl1',
//   action: 'inviteToGroup' }

// 그룹 : 퇴장
//  { content: '',
//   buddyId: 'BU_1M9qHDITjlU0',
//   groupId: 'GID_ihTl1',
//   action: 'exitFromGroup' }

// 공통 명령어(권장)
// 시작		start	:  중지 상태인 봇을 재개 합니다.
// 끝		stop	:  봇을 잠시 중지 시킵니다.
// 퇴장		exit	:  그룹대화에서 봇을 퇴장 시킵니다. 단 1:1 대화에서 동작하지 않습니다.
// 도움말	help	:  봇 사용 방법을 보여줍니다.


var MSG = {
	HELP : [
		'안녕하세요, 일본어 단어암기 봇입니다.',
		'제가 내는 문제를 맞춰보세요~',
		'',
		'명령어)',
		'* 도움말 : 지금 보고계시는 도움말을 다시 볼 수 있습니다',
		'* 퇴장 : 제가 그룹 대화방에서 나가게 됩니다',
		'',
		'* 카나 : 제가 히라가나/가타카나를 제시하면 뜻을 입력 해주세요',
		'* 한자 : 제가 한자를 제시하면 뜻을 입력 해주세요',
		'* 뜻   : 제가 뜻을 제시하면 카나 또는 한자를 입력 해주세요'
	].join('\n'),

	BYE : '다음에 또 초대 해 주세요~ 안녕~',

	KANA_CONV : '이제부터 제가 히라가나/가타카나를 얘기 할테니 뜻을 재빨리 입력 해주세요!',
	KANJI_CONV : '이제부터 제가 한자를 얘기 할테니 뜻을 재빨리 입력 해주세요!',
	MEAN_CONV : '이제부터 제가 뜻을 얘기 할테니 히라가나/가타카나 또는 한자를 재빨리 입력 해주세요!'
}

var quizes = [
	{
		kana : 'あさって',
		kanji : '明後日',
		mean : '내일모레|모레'
	},
	{
		kana : 'あした',
		kanji : '明日',
		mean : '내일'
	}
];


var states = {};

function getRandomQuiz(mode) {

	var quiz;

	do {
		quiz = quizes[Math.floor(Math.random() * quizes.length)];
	} while (!quiz[mode])

	return quiz;

}

function getQuizMessage(state) {

	var mode = state.mode;
	var quiz = state.quiz = getRandomQuiz(mode);

	var question = quiz[mode].split('|');

	switch (mode) {
	case 'kana':
	case 'kanji':
		return [
			'문제 : ' + question,
			'위 문제의 뜻을 입력 해주세요!'
		].join('\n');

	case 'mean':
		return [
			'문제 : ' + question,
			'위와 같은 뜻을 가진 단어를 입력 해주세요!'
		].join('\n');
		break;
	}

}

function getAnswers(state) {

	var mode = state.mode;
	var quiz = state.quiz;

	switch (mode) {
	case 'kana':
	case 'kanji':
		return quiz.mean.split('|');

	case 'mean':
		return quiz.kana.split('|').concat(quiz.kanji.split('|'));
	}

}

exports.callback = function(req, res, options){

	var params = eval(req.body);
	params['content'] = params['content'].trim();

	var whose = params['groupId'] + ':' + params['buddyId'];
	var state = states[whose] = states[whose] || { mode : 'kana', quiz : null };

	var mode = state.mode;
	var quiz = state.quiz;

	var replys = [];

	// 파라미터 출력
	console.log('\nPOST /callback\n',params,'\n');

	switch (params['action']) {
	case 'sendFromMessage':

		console.log('###', params['content']);

		switch (params['content']) {
		case '도움말':
			receiver.say(params, MSG.HELP);
			receiver.say(params, getQuizMessage(state))
			break;

		case '퇴장':
			receiver.exitBot(params.groupId);
			break;

		case '카나':
			state.mode = 'kana';
			receiver.say(params, MSG.KANA_CONV);
			receiver.say(params, getQuizMessage(state));
			break;

		case '한자':
			state.mode = 'kanji';
			receiver.say(params, MSG.KANA_CONV);
			receiver.say(params, getQuizMessage(state));
			break;

		case '뜻':
			state.mode = 'mean';
			receiver.say(params, MSG.KANA_CONV);
			receiver.say(params, getQuizMessage(state));
			break;

		default:

			console.log('state.quiz : ', state.quiz);

			if (state.quiz) {

				var answers = getAnswers(state);
				var correct = answers.indexOf(params['content']) > -1;

				if (correct) {
					receiver.say(params, '쩔긔; 맞았어요!');
				} else {
					receiver.say(params, '틀렸어요 ㅠㅠ 정답은 "' + answers.join(', ') + '"" 입니다;');
				}

			}

			receiver.say(params, getQuizMessage(state));

		}

		break;

	case 'inviteGroup':
		receiver.say(params, MSG.HELP);
		receiver.say(params, getQuizMessage(state))
		break;

	case 'exitFromGroup':
		receiver.say(params, MSG.BYE);
		break;
	}

	// if (isGroup && /^(나가)$/.test(params['content'])) {
	// 	receiver.sendFromGroup(params['groupId'], params['buddyId'], '다음에 또 초대 해 주세요~ 안녕~');
	// 	receiver.exitBot(params['groupId']);
	// }

	// // 그룹방
	// if(params["groupId"]){

	// 	if(params["content"] === "나가" || params["content"] === "꺼져") {
	// 		receiver.exitBot(params["groupId"]);
	// 	/*}else if(params["content"] === "테스트") {
	// 		receiver.groupTest(params["groupId"], params["buddyId"], params["content"], '[{"name":"초대자"}]');*/
	// 	}else if(params["content"] === "그룹목록") {			
	// 		receiver.getMembers(params["groupId"]);
	// 	}else if(params["action"] === "addBuddy") {
	// 		receiver.addBuddy(params["buddyId"]);
	// 	}else if(params["action"] === "sendFromMessage") {
	// 		receiver.sendFromMessage(params["buddyId"], params["content"]);
	// 	}else if(params["action"] === "sendFromGroup") {
	// 		receiver.sendFromGroup(params["groupId"], params["buddyId"], params["content"]);
	// 	}else if(params["action"] === "createGroup") {
	// 		receiver.createGroup(params["groupId"], params["buddyId"]);
	// 	}else if(params["action"] === "inviteToGroup") {
	// 		receiver.inviteToGroup(params["groupId"], params["buddyId"], params["content"]);
	// 	}else if(params["action"] === "exitFromGroup") {
	// 		receiver.exitFromGroup(params["groupId"], params["buddyId"]);
	// 	}
			
	// }

	// // 개인방
	// if(!params["groupId"]){

	// 	/*if(params["content"] === "테스트") {
	// 		receiver.buddyTest(params["buddyId"], params["content"]);
	// 	}else */if(params["content"] === "이미지") {
	// 		receiver.sendFromImage(params["buddyId"]);
	// 	}else if(params["content"] === "프로필이미지") {
	// 		receiver.profileDownload(params["buddyId"]);
	// 	}else if(params["action"] === "addBuddy") {
	// 		receiver.addBuddy(params["buddyId"]);
	// 	}else if(params["action"] === "sendFromMessage") {
	// 		receiver.sendFromMessage(params["buddyId"], params["content"]);
	// 	}

	// }

	console.log('answer : ', replys);

	receiver.flush();
	// receiver.say(params, replys.join('\n\n'));
	res.end();
};
